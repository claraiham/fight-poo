import Warrior from "./warrior.js";
import WarriorAxe from "./WarriorAxe.js";
import WarriorSpear from "./WarriorSpear.js";
import WarriorSword from "./WarriorSword.js";


let Merlin = new WarriorSpear("Merlin", 5, 100)
let Adibou = new WarriorSword("Adibou", 12, 100)
let Siegfried = new WarriorAxe("Siegfried", 13, 100)

Merlin.attack(Adibou)
console.log(Adibou.isAlive())


//créer une boucle de combat

function fight(warrior1, warrior2, warrior3){
    do{

        warrior1.attack(warrior2)
        warrior2.attack(warrior1)
        warrior1.attack(warrior3)
        warrior3.attack(warrior1)
        warrior2.attack(warrior3)
        warrior3.attack(warrior2)
    
    }while(warrior1.isAlive() === true || warrior2.isAlive() === true|| warrior3.isAlive() === true )
    
    if(warrior1 || warrior2 || warrior3 > 0){
        return `${warrior1.name || warrior2.name || warrior3.name} is the winner !`
    }
}

console.log(fight(Merlin, Adibou, Siegfried))