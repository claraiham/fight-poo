import Warrior from "./warrior.js"
import WarriorAxe from "./WarriorAxe.js"

class WarriorSpear extends Warrior{
    constructor(name, power, life){
        super(name, power, life)
    }
    attack(opponent){
        if(opponent instanceof WarriorAxe){
            opponent.life = opponent.life - this.power*2
        }else{
            return super.attack(opponent)
        } 
    }
}

export default WarriorSpear