import Warrior from "./warrior.js"
import WarriorSpear from "./WarriorSpear.js"

class WarriorSword extends Warrior{
    constructor(name, power, life){
        super(name, power, life)
    }
    attack(opponent){

        if( opponent instanceof WarriorSpear){
            opponent.life = opponent.life - this.power*2  
        }else{
            return super.attack(opponent)
        }
    }
}

export default WarriorSword